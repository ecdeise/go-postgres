import React, { Component, Fragment} from 'react';
import './AppFooter.css'

export default class AppFooter extends Component{

    render(){
        const currentYear = new Date().getUTCFullYear();
        return(
            <Fragment>
            <hr />
            <p className="footer">Copyright &copy; 2020 - {currentYear} deise-labs.org</p>
            </Fragment>
        );
    }
}