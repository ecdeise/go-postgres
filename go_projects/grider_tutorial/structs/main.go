package main

import "fmt"

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contactInfo
}

func main() {
	// Way 1
	// positional - order dependent for assignment
	//alex := person{"Alex", "Anderson"}

	// Way 2
	// non-positional assignment
	// alex := person{firstName: "Alex", lastName: "Anderson"}
	// fmt.Println(alex)

	jim := person{
		firstName: "Jim",
		lastName:  "Party",
		contactInfo: contactInfo{
			email:   "jim.party@test.com",
			zipCode: 12345,
		},
	}

	jim.updateName("Jimmyjim")
	jim.print()

}

// Pointers
func (pointerToPerson *person) updateName(newFirstName string) {
	(*pointerToPerson).firstName = newFirstName
}

func (p person) print() {
	fmt.Printf("%+v\n", p)
}
