package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	// get args passed in when running go run main.go myfile.txt
	// prints out the source file arg -> myfile.txt
	//fmt.Println(os.Args)
	filename := os.Args[1]
	fmt.Println("Filename: ", filename)

	f, err := os.Open(filename)
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1)
	}

	io.Copy(os.Stdout, f)

}
