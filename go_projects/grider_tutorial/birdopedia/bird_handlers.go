package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Bird struct {
	Species     string `json:"species"`
	Description string `json:"description"`
}

var birds []Bird

func getBirdHandler(w http.ResponseWriter, r *http.Request) {
	//convert birds to json
	birdListBytes, err := json.Marshal(birds)

	// catch any errors
	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// happy path write json list of birds to response
	w.Write(birdListBytes)
}

func createBirdHandler(w http.ResponseWriter, r *http.Request) {
	bird := Bird{}

	//parse the form request
	err := r.ParseForm()

	//catch errors and return to user
	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// parse info from form
	bird.Species = r.Form.Get("species")
	bird.Description = r.Form.Get("description")

	//append new bird to the slice
	birds = append(birds, bird)

	// redirect user to the assets directory
	http.Redirect(w, r, "/assets/", http.StatusFound)
}
