package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// new router function create router and returns it
// assists in testability
func newRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/hello", handler).Methods("GET")

	// static file directory
	staticFileDirectory := http.Dir("./assets")
	// strip the prefix
	staticFileHandler := http.StripPrefix("/assets/", http.FileServer(staticFileDirectory))

	// pathprefix method acts to match all routes starting
	// with "/assets/" and not the absolute route
	r.PathPrefix("/assets/").Handler(staticFileHandler).Methods("GET")
	r.HandleFunc("/bird", getBirdHandler).Methods("GET")
	r.HandleFunc("/bird", createBirdHandler).Methods("POST")

	return r
}

func main() {
	// call newRouter function and pass to listen/server
	r := newRouter()
	http.ListenAndServe(":8080", r)
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}
