package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://stackoverflow.com",
		"http://golang.org",
		"http://amazon.com",
	}

	// make a channel for go routine wrangling
	c := make(chan string)

	for _, link := range links {
		go checkLink(link, c)
	}

	// watch the channel c, when a value appears
	// assign it to value l (link) and then execute
	// the body of the for loop
	for l := range c {
		// function literal - unname function
		// same as lamda or anonymous func
		go func(link string) {
			time.Sleep(2 * time.Second)
			go checkLink(link, c)
		}(l) //pass in the link so that as l changes in outer loop, the go routine will reference the proper link
	}
}

func checkLink(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, "might be down!")
		c <- link
		return
	}
	fmt.Println(link, "is up!")
	c <- link
}
