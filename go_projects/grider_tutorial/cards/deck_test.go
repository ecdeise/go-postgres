package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	if len(d) != 16 {
		t.Errorf("Expected deck length of 16, but got %v", len(d))
	}

	if d[0] != "Ace of Spades" {
		t.Errorf("Expected Ace of Spades, but got %v", d[0])
	}

	if d[len(d)-1] != "Four of Clubs" {
		t.Errorf("Expected Four of Clubs, but got %v", d[len(d)-1])
	}
}

func TestSaveToDeckAndNewDeckFromFile(t *testing.T) {
	// pre cleanup - remove if exists
	os.Remove("_decktesting")

	// create new deck
	deck := newDeck()

	// save it to file
	deck.saveToFile("_decktesting")

	// load from file
	loadedDeck := newDeckFromFile("_decktesting")

	// assert length is what it should be
	if len(loadedDeck) != 16 {
		t.Errorf("Expected 16 cards, got %v", len(loadedDeck))
	}

	// post cleanup
	os.Remove("_decktesting")
}
