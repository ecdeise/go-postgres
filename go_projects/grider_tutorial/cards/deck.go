package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

type deck []string

//returns something of type deck
func newDeck() deck {
	cards := deck{}

	cardSuits := []string{"Spades", "Diamonds", "Hearts", "Clubs"}
	cardValues := []string{"Ace", "Two", "Three", "Four"}

	// two loops to build a deck from suits and values
	for _, suit := range cardSuits {
		for _, value := range cardValues {
			cards = append(cards, value+" of "+suit)
		}
	}
	return cards
}

func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

// return two values from function both of type deck
func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

// reciever function take a deck to a slice of string (for file save)
// convert the slice of string to string with join
func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

// save deck to file, returns error if applicable
// perms anyone can read and write this file
func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

func newDeckFromFile(filename string) deck {
	bs, err := ioutil.ReadFile(filename)

	//if we have an error - log error and exit (option 2)
	if err != nil {
		// option 1 - log error and return a call to newDeck()
		// option 2 - log error and quite the program
		fmt.Println("Error:", err)
		// exit program
		os.Exit(1)
	}

	// otherwise it worked
	// turn byte slice into slice of strings, split that string on "c" and set to s
	s := strings.Split(string(bs), ",")
	// turn slice of strings into a deck
	return deck(s)

}

// shuffle - not built in slice shuffle
// for each index, card in cards
// generate a random # between 0 and len(cards)-1
// swap current card and card at cards[randomNumber]
func (d deck) shuffle() {

	// for better random # generation
	// use time to get better rando based on the current time
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		// swap i and newposition
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}
