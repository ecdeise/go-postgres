package main

import "fmt"

func main() {
	cards := newDeck()
	cards.print()
	fmt.Println("-----------")
	//cards.saveToFile("my_cards")
	//cards := newDeckFromFile("my_")
	cards.shuffle()
	cards.print()

}
